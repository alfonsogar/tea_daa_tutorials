## Tutorials for Data Acquisition, analyses and methods in life sciences

All the tutorials for the subject "Data acquisition, analyses and scientific methods in life sciences" in the Plant Healt Master at Universitat Politècnica de València. c 
They should be also useful  for self-learning of R.

## Protocol

1. To download the files copy-paste the clone link into console. Once into the folder open the project in RStudio and render the files in the desired format. 

1. Render all tutorials (*\*_Tutorial-\*.Rmd*) using the script *00_Render-Rmd-files.R*. It produces the formats included in yaml headers: *.html, .doc* and *.pdf*.

```
source("00_Render-Rmd-files.R")
rmarkdown::render_site()
```

Or one  by one: 

```
library(rmarkdown)
rmarkdown::render("01_Tutorial-1_R-easy-start.Rmd", "all")
rmarkdown::render("01_Tutorial-1_R-easy-start.Rmd", 
   output_format = "word_document")
rmarkdown::render("02_Tutorial-2_R-operations.Rmd", "all")
rmarkdown::render("03_Tutorial-3_R-base-graphics.Rmd", "all")
rmarkdown::render("04_Tutorial-4_R-Quarto-Rmarkdown.Rmd", "all")
rmarkdown::render("05_Tutorial-5_R-hypotheses-testing.Rmd", "all")
rmarkdown::render("06_Tutorial-6_R-ggplot.Rmd", "all")  # pdf and html only
rmarkdown::render("07_Tutorial-7_R-dplyr-tidyr.Rmd", "all")  
```

3. To produce epub or other fancy formats it is possible using pandoc in the Terminal (need to install pandoc previously). 

```{bash}
pandoc --mathjax -s --highlight-style tango 01_Tutorial-1_R-easy-start.html --to epub -o 01_Tutorial-1_R-easy-start.epub

pandoc --mathjax -s --highlight-style tango 02_Tutorial-2_R-operations.html --to epub -o 02_Tutorial-2_R-operations.epub

pandoc --mathjax -s --highlight-style tango 03_Tutorial-3_R-base-graphics.html --to epub -o 03_Tutorial-3_R-base-graphics.epub

pandoc --mathjax -s --highlight-style tango 04_Tutorial-4_R-Quarto-Rmarkdown.html --to epub -o 04_Tutorial-4_R-Quarto-Rmarkdown.epub

pandoc --mathjax -s --highlight-style tango 04_Tutorial-4_R-Quarto-Rmarkdown.html --to docx -o 04_Tutorial-4_R-Quarto-Rmarkdown.docx --reference-doc=templates/template.docx

pandoc --mathjax -s --highlight-style tango 05_Tutorial-5_R-hypotheses-testing.html --to epub -o 05_Tutorial-5_R-hypotheses-testing.epub

pandoc --mathjax -s --highlight-style tango 06_Tutorial-6_R-ggplot.html --to epub -o 06_Tutorial-6_R-ggplot.epub

pandoc --mathjax -s --highlight-style tango 06_Tutorial-6_R-ggplot.html --to docx -o 06_Tutorial-6_R-ggplot.docx --reference-doc=templates/template.docx

pandoc --mathjax -s --highlight-style tango 07_Tutorial-7_R-dplyr-tidyr.html --to epub -o 07_Tutorial-7_R-dplyr-tidyr.epub

```
x. Extrat R code. 

```
dir()
library(knitr)
purl("01_Tutorial-1_R-easy-start.Rmd")
purl("02_Tutorial-2_R-operations.Rmd")
purl("03_Tutorial-3_R-base-graphics.Rmd")
purl("04_Tutorial-4_R-RMarkdown.Rmd")
purl("05_Tutorial-5_R-hypotheses-testing.Rmd")
purl("06_Tutorial-6_R-ggplot.Rmd")
purl("07_Tutorial-7_R-dplyr-tidyr.Rmd")

```

4. Move by hand the files changed to Drive.

5. Source *00_Drive_upload.R* to ~~upload results to Drive and~~ move all results to *results/* folder.

6. To add (or update) a tutorial in the web (**only for author**): 
    - Update the .html tutorial in the folder
    *smb://UPVNET;algarsal@nasupv/discos/a/algarsal/www/R-tutorials/*.
    
    - Change if necessary the link in the blog:
        *https://garmendia.blogs.upv.es/r-lecture-notes/*
        
    - Update other formats folder *TEA_DAA_tutorials-other-formats* in Drive:
    https://drive.google.com/drive/folders/19w914WCg8BVTVBE_zpgShmg2vpjguV1e

    - Remember do not put exams or results into this repository. It is public. Use instead *TAA_DAA_Tutorials-ANSWERS*.

## Description

The folders structure is the standard for R projects: 

- *biblio*, for .bib and .csl files for creating bibliographies.
- *R*, all functions and setup files that need to be activated from the beginning. 
- *results*, where all results, included all figures should go. Not sync with repository.
- *templates*, for templates to different document types. 
- *images*, for images used in tutorials.

Core files are: 

- *README.md*, this file, to explain structure of the project. 
- *00_Drive_upload.R* file is used to upload documents to GDrive results folder. 
- *\*_Tutorial-\*.Rmd*  files for each tutorial. 

## repository
git@bitbucket.org:alfonsogar/TEA_DAA_Tutorials.git
https://algarsal@bitbucket.org/alfonsogar/tea_daa_tutorials.git

Drive folder:
https://drive.google.com/drive/folders/1sb4rS7BwERgp7BGx_ZxcQvOkZhsGd5YG

Drive results:
https://drive.google.com/drive/folders/1Katl9q5CN5RUvivMBFBYLWnhaukdwI8P

Repository for answers: 
git@bitbucket.org:alfonsogar/TEA_DAA_Tutorials-ANSWERS.git

## To do

1. Fix 00a_Render-Rmd-files.R to: 
    - render the html files in the smb folder directly.
    - render other html file in other folder. With more web format. See: 
      https://stackoverflow.com/questions/24610740/rmarkdown-different-output-folders-shared-libs?rq=1
      https://prettydoc.statr.me/
    - Pandoc the epub and other formats directly from a for loop using the names of the Rmd files. 
    
1. Create new tutorials:
https://docs.google.com/document/d/1j-EpBmB8Ny4LEqKPh_QBKKIrlKVo3q6iZv0iW70Rsno/edit

1. Revise tutorials and improve using:
    https://github.com/Rstats-courses/CEA2018/blob/master/intro/Intro.Rpres

